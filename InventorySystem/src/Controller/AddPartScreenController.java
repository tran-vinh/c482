package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Inhouse;
import Model.Inventory;
import static Model.Inventory.addPart;
import Model.Outsourced;
import Model.Part;

public class AddPartScreenController implements Initializable {
    
    @FXML
    private AnchorPane scrAddPart;

    @FXML
    private RadioButton rbAddPartInHouse;

    @FXML
    private ToggleGroup tgSource;

    @FXML
    private RadioButton rbAddPartOutsourced;

    @FXML
    private Label lblAddPartMachineId;

    @FXML
    private TextField txtAddPartID;

    @FXML
    private TextField txtAddPartName;

    @FXML
    private TextField txtAddPartInv;

    @FXML
    private TextField txtAddPartPrice;

    @FXML
    private TextField txtAddPartMax;

    @FXML
    private TextField txtAddPartMachineId;

    @FXML
    private TextField txtAddPartMin;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;
    
    @FXML
    private boolean bOutSourced;
    
    @FXML
    private String partParseError = new String();
    
    @FXML
    private String partFieldError = new String();
    
    @FXML
    private int id;

    @FXML
    void handleAddPartCancel(ActionEvent event) throws IOException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Cancel Add Part");
            alert.setHeaderText("Are you sure you want to cancel? Click OK to return to Main screen. Click Cancel to stay on this screen.");
            alert.showAndWait();
            
            if (alert.getResult() == ButtonType.OK) {
                Parent root = FXMLLoader.load(getClass().getResource("/View/MainScreen.fxml"));
                Scene scene = new Scene(root);        
                Stage winMainScreen = (Stage)((Node)event.getSource()).getScene().getWindow();        
                winMainScreen.setTitle("Inventory Management System");
                winMainScreen.setScene(scene);
                winMainScreen.show();
            }
            else {
                alert.close();
            }
        }
        catch (IOException e) {}
    }

    @FXML
    void handleAddPartSave(ActionEvent event) throws IOException {
        String name = txtAddPartName.getText();
        String stockStr = txtAddPartInv.getText();
        String priceStr = txtAddPartPrice.getText();
        String minStr = txtAddPartMin.getText();
        String maxStr = txtAddPartMax.getText();
        String machineIdStr = txtAddPartMachineId.getText();
        
        String partErrors = Part.partCheck(name, stockStr, priceStr, maxStr, minStr, machineIdStr);
        if(bOutSourced != true) {
            try {
                if(!machineIdStr.equals("")) Integer.parseInt(machineIdStr); 
            } catch (NumberFormatException e) {
                partErrors += "The Machine Id for In House parts must be a number. \n";
            }     
        }
        
        if (partErrors.length() > 0){
           Alert alert = new Alert(Alert.AlertType.WARNING);
           alert.setTitle("Add Part Error");
           alert.setHeaderText("Part not added.");
           alert.setContentText(partErrors);
           alert.showAndWait();
           return;
        }
        
        int stock = Integer.parseInt(stockStr);
        double price = Double.parseDouble(priceStr);
        int min = Integer.parseInt(minStr);
        int max = Integer.parseInt(maxStr);
        
        if (bOutSourced == true) {
            addPart(new Outsourced(
                    Inventory.nextPartId(), 
                    name, 
                    price,
                    stock,
                    min, 
                    max, 
                    machineIdStr                        
                )
            );
        } else {
            int machineId = Integer.parseInt(machineIdStr);
            addPart(new Inhouse(
                    Inventory.nextPartId(), 
                    name, 
                    price,
                    stock,
                    min, 
                    max, 
                    Integer.parseInt(machineIdStr) 
                )
            );
        }    
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MainScreen.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage winMainScreen = (Stage)((Node)event.getSource()).getScene().getWindow();
            winMainScreen.setTitle("Inventory Management System");
            winMainScreen.setScene(scene);
            winMainScreen.show();
        }catch(IOException e) {}
    }
    
    @FXML
    void setTGSource(ActionEvent event) {
        if (tgSource.getSelectedToggle() == rbAddPartInHouse) {
            lblAddPartMachineId.setText("Machine ID");
            txtAddPartMachineId.setPromptText("Mach ID");
            bOutSourced = false;  
        }
        else {
            lblAddPartMachineId.setText("Company Name");
            txtAddPartMachineId.setPromptText("Comp Nm");
            bOutSourced = true;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtAddPartID.setText("Auto Gen - Disabled");
    }
}
