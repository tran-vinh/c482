package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Inventory;
import static Model.Inventory.getAllParts;
import Model.Part;
import Model.Product;
import static Model.Inventory.lookupPart;


public class AddProductScreenController implements Initializable {
    Product addProduct = new Product();
        
    @FXML
    private AnchorPane scrAddProduct;

    @FXML
    private TextField txtAddProductID;

    @FXML
    private TextField txtAddProductName;

    @FXML
    private TextField txtAddProductInv;

    @FXML
    private TextField txtAddProductPrice;

    @FXML
    private TextField txtAddProductMax;

    @FXML
    private TextField txtAddProductMin;

    @FXML
    private Button btnSearch;

    @FXML
    private TextField txtSearchParts;

    @FXML
    private TableView<Part> tvPartsInStock;

    @FXML
    private TableColumn<Part, Integer> partIDCol;

    @FXML
    private TableColumn<Part, String> partNameCol;

    @FXML
    private TableColumn<Part, Integer> partInStockCol;

    @FXML
    private TableColumn<Part, Double> partPriceCol;

    @FXML
    private Button btnAdd;

    @FXML
    private TableView<Part> tvAssociatedParts;

    @FXML
    private TableColumn<Part, Integer> associatedPartIDCol;

    @FXML
    private TableColumn<Part, String> associatedPartNameCol;

    @FXML
    private TableColumn<Part, Integer> associatedPartInStockCol;

    @FXML
    private TableColumn<Part, Double> associatedPartPriceCol;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;
    
    @FXML
    private int productID;
    
    @FXML
    private String productParseError = new String();
    
    @FXML
    private String productFieldError = new String();
    
    @FXML
    public void handleSearchAction(ActionEvent event) {
        String searchPartStr = txtSearchParts.getText();
        ObservableList<Part> searchPartList = FXCollections.observableArrayList();
        try{
            searchPartList.add(lookupPart(Integer.parseInt(searchPartStr)));
            tvPartsInStock.setItems(searchPartList);
        }
        catch(NumberFormatException e){
            searchPartList.add(lookupPart(searchPartStr));
            tvPartsInStock.setItems(searchPartList);
        }
        finally {
            if(searchPartStr.equals("")) {
                tvPartsInStock.setItems(getAllParts());
            }
        }
    }
    
    @FXML
    public void handleAddAction(ActionEvent event) {
        Part part = tvPartsInStock.getSelectionModel().getSelectedItem();
        if (part != null) {
            addProduct.addAssociatedPart(part);
            tvAssociatedParts.setItems(addProduct.getAllAssociatedParts());
        }
    }
    
    @FXML
    public void handleDeleteAction(ActionEvent event) {
        if (tvAssociatedParts.getSelectionModel().getSelectedIndex() != -1) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Associated Part");
            alert.setHeaderText("Are you sure you want to delete the associated part? Click OK to delete the associated part. Click Cancel to return to product screen.");
            alert.showAndWait();
            
            if (alert.getResult() == ButtonType.OK) {
                Part part = tvAssociatedParts.getSelectionModel().getSelectedItem();
                addProduct.deleteAssociatedPart(part);
                tvAssociatedParts.setItems(addProduct.getAllAssociatedParts());
            }
            else {
                alert.close();
            }
        }
    }
    
    @FXML
    public void handleSaveAction(ActionEvent event) throws IOException {
        String name = txtAddProductName.getText();
        String stockStr = txtAddProductInv.getText();
        String priceStr = txtAddProductPrice.getText();
        String maxStr = txtAddProductMax.getText();
        String minStr = txtAddProductMin.getText();
        ObservableList<Part> associatedParts = tvAssociatedParts.getItems();
        
        String productErrors = Product.productCheck(name, stockStr, priceStr, maxStr, minStr);
        
        if (tvAssociatedParts.getItems().isEmpty()) {
            productErrors += "No associated parts.\n";
        }
        
        if (productErrors.length() > 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Modify Product Error");
            alert.setHeaderText("Product not saved.");
            alert.setContentText(productErrors);
            alert.showAndWait();
            return;
            
        }else {
            addProduct.setId(Inventory.nextProductId());
            addProduct.setName(name);
            addProduct.setStock(Integer.parseInt(stockStr));
            addProduct.setPrice(Double.parseDouble(priceStr));
            addProduct.setMax(Integer.parseInt(maxStr));
            addProduct.setMin(Integer.parseInt(minStr));
            Inventory.addProduct(addProduct);
            try{
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MainScreen.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage winMainScreen = (Stage)((Node)event.getSource()).getScene().getWindow();
                winMainScreen.setTitle("Inventory Management System");
                winMainScreen.setScene(scene);
                winMainScreen.show();
            } catch(IOException e){}
        }
    }
    
    @FXML
    public void handleCancelAction (ActionEvent eCancel) throws IOException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Cancel Add Product");
            alert.setHeaderText("Are you sure you want to cancel? Click OK to exit to the Main screen. Click Cancel to stay.");
            alert.showAndWait();
            
            if (alert.getResult() == ButtonType.OK) {
                Parent root = FXMLLoader.load(getClass().getResource("/View/MainScreen.fxml"));
                Scene scene = new Scene(root);
                Stage winMainScreen = (Stage)((Node)eCancel.getSource()).getScene().getWindow();
                winMainScreen.setTitle("Inventory Management System");
                winMainScreen.setScene(scene);
                winMainScreen.show();
            }
            else {
                alert.close();
            }
        }catch (IOException e) {}
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        partIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        
        associatedPartIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        associatedPartNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        associatedPartInStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        associatedPartPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        
        txtAddProductID.setText("Auto Gen - Disabled"); 
        tvPartsInStock.setItems(getAllParts());

    }    
    
}
