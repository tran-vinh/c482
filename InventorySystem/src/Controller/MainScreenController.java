package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Inventory;
import static Model.Inventory.deletePart;
import Model.Part;
import Model.Product;
import static Model.Inventory.deleteProduct;
import static Model.Inventory.getAllParts;
import static Model.Inventory.getAllProducts;
import static Model.Inventory.lookupProduct;
import static Model.Inventory.lookupPart;

public class MainScreenController implements Initializable {

    @FXML
    private AnchorPane scrMain;

    @FXML
    private Button btnExit;

    
    @FXML
    private TextField txtSearchParts;
    
    @FXML
    private TextField txtSearchProducts;
    
    @FXML
    private TableView<Part> tvParts;

    @FXML
    private TableColumn<Part, Integer> partIDCol;

    @FXML
    private TableColumn<Part, String> partNameCol;

    @FXML
    private TableColumn<Part, Integer> partInStockCol;

    @FXML
    private TableColumn<Part, Double> partPriceCol;

    @FXML
    private TableView<Product> tvProducts;

    @FXML
    private TableColumn<Product, Integer> productIDCol;

    @FXML
    private TableColumn<Product, String> productNameCol;

    @FXML
    private TableColumn<Product, Integer> productInventoryCol;

    @FXML
    private TableColumn<Product, Double> productPriceCol;
    @FXML
    private Button btnAddParts;
    @FXML
    private Button btnModifyParts;
    @FXML
    private Button btnDeleteParts;
    @FXML
    private Button btnSearchParts;
    @FXML
    private Button btnAddProducts;
    @FXML
    private Button btnModifyProducts;
    @FXML
    private Button btnDeleteProducts;
    @FXML
    private Button btnSearchProducts;
    
    @FXML
    public void addPartsAction (ActionEvent eAddParts) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/AddPartScreen.fxml"));
            Parent addPartScreen = loader.load();
            Scene addPartScene = new Scene(addPartScreen);
            Stage winAddPart = (Stage)((Node)eAddParts.getSource()).getScene().getWindow();
            winAddPart.setTitle("Add Part");
            winAddPart.setScene(addPartScene);
            winAddPart.show();
        }
        catch (IOException e) {}
    }
    
    @FXML
    public void modifyPartsAction (ActionEvent eModifyParts) throws IOException {
        selectedPart = tvParts.getSelectionModel().getSelectedItem();
        selectedPartIndex = getAllParts().indexOf(selectedPart);
        if (selectedPart == null) {
            Alert nullalert = new Alert(AlertType.ERROR);
            nullalert.setTitle("Modify Part Error");
            nullalert.setHeaderText("No part was selected.");
            nullalert.showAndWait();
        }
        else {
            try {
                Parent modifyPartScreen = FXMLLoader.load(getClass().getResource("/View/ModifyPartScreen.fxml"));
                Scene modifyPartScene = new Scene(modifyPartScreen);
                Stage winModifyPart = (Stage)((Node)eModifyParts.getSource()).getScene().getWindow();
                winModifyPart.setTitle("Modify Part");
                winModifyPart.setScene(modifyPartScene);
                winModifyPart.show();
            }
            catch (IOException e) {}
        }
    }
    
    @FXML
    public void deletePartsAction(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete Part");
        alert.setHeaderText("Are you sure you want to delete this part? Click OK to delete or click Cancel to go back.");
        alert.showAndWait();
        
        if (alert.getResult() == ButtonType.OK) {
            try {
                Part deletedPart = tvParts.getSelectionModel().getSelectedItem();
                deletePart(deletedPart);
            }
            catch (NullPointerException e) {
                Alert nullalert = new Alert(AlertType.ERROR);
                nullalert.setTitle("Delete Part Error");
                nullalert.setHeaderText("No part was selected.");
                nullalert.showAndWait();
            }
        }
        else {
            alert.close();
        }
    }
    
    @FXML
    public void searchPartsAction(ActionEvent event) {
        String searchPartStr = txtSearchParts.getText();
        ObservableList<Part> searchPartList = FXCollections.observableArrayList();
        try{
            searchPartList.add(lookupPart(Integer.parseInt(searchPartStr)));
            tvParts.setItems(searchPartList);
        }
        catch(NumberFormatException e){
            searchPartList.add(lookupPart(searchPartStr));
            tvParts.setItems(searchPartList);
        }
        finally {
            if(searchPartStr.equals("")) {
                updatePartsTV();
            }
        }
    }        
    
    @FXML
    public void addProductsAction (ActionEvent eAddProducts) throws IOException {
        try {
            Parent addProductScreen = FXMLLoader.load(getClass().getResource("/View/AddProductScreen.fxml"));
            Scene addProductScene = new Scene(addProductScreen);
            Stage winAddProduct = (Stage)((Node)eAddProducts.getSource()).getScene().getWindow();
            winAddProduct.setTitle("Add Product");
            winAddProduct.setScene(addProductScene);
            winAddProduct.show();
        }
        catch (IOException e) {}
    }
    
    @FXML
    public void modifyProductsAction (ActionEvent eModifyProducts) throws IOException {
        selectedProduct = tvProducts.getSelectionModel().getSelectedItem();
        selectedProductIndex = getAllProducts().indexOf(selectedProduct);
        if (selectedProduct == null) {
            Alert nullalert = new Alert(AlertType.ERROR);
            nullalert.setTitle("Modify Product Error");
            nullalert.setHeaderText("No product selected.");
            nullalert.showAndWait();
        }
        else {
            try {
                Parent modifyProductScreen = FXMLLoader.load(getClass().getResource("/View/ModifyProductScreen.fxml"));
                Scene modifyProductScene = new Scene(modifyProductScreen);
                Stage winModifyProduct = (Stage)((Node)eModifyProducts.getSource()).getScene().getWindow();
                winModifyProduct.setTitle("Modify Product");
                winModifyProduct.setScene(modifyProductScene);
                winModifyProduct.show();
            }
            catch (IOException e) {}
        }
    }
    
    @FXML
    public void deleteProductsAction(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Delete Product");
        alert.setHeaderText("Are you sure you want to delete this product? Click OK to delete or click Cancel to go back.");
        alert.showAndWait();
        
        if (alert.getResult() == ButtonType.OK) {
            Product deletedProduct = tvProducts.getSelectionModel().getSelectedItem();
            deleteProduct(deletedProduct);
        }
        else {
            alert.close();
        }
    }

    @FXML
    public void searchProductsAction(ActionEvent event) {
            String searchProductStr = txtSearchProducts.getText();
            ObservableList<Product> searchProductList = FXCollections.observableArrayList();
        try{
            searchProductList.add(lookupProduct(Integer.parseInt(searchProductStr)));
            tvProducts.setItems(searchProductList);
        }
        catch(NumberFormatException e){
            searchProductList.add(lookupProduct(searchProductStr));
            tvProducts.setItems(searchProductList);
        }
        finally {
            if(searchProductStr.equals("")) {
                updateProductsTV();
            }
        }
    }
    
    @FXML
    public void updatePartsTV() {
        tvParts.setItems(getAllParts());
    }
    @FXML
    public void updateProductsTV() {
        tvProducts.setItems(getAllProducts());
    }
    
    @FXML
    public void exitButtonAction (ActionEvent eExitButton) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit");
        alert.setHeaderText("Are you sure you want to exit? Click OK to close or click Cancel to go back.");
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            Stage winMainScreen = (Stage)((Node)eExitButton.getSource()).getScene().getWindow();
            winMainScreen.close();
        }
        else {
            alert.close();
        }
    }
    
    private static Part selectedPart;
    
    private static int selectedPartIndex;
    
    public static Part getSelectedPart() {
        return selectedPart;
    }
    
    public static int getSelectedPartIndex() {
        return selectedPartIndex;
    }
    
    private static Product selectedProduct;
    
    private static int selectedProductIndex;
    
    public static Product getSelectedProduct() {
        return selectedProduct;
    }
    
    public static int getSelectedProductIndex() {
        return selectedProductIndex;
    }
    
    public static ObservableList<Part> selectedAssocPart = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        partIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        updatePartsTV();
        
        productIDCol.setCellValueFactory(new PropertyValueFactory("id"));
        productNameCol.setCellValueFactory(new PropertyValueFactory("name"));
        productInventoryCol.setCellValueFactory(new PropertyValueFactory("stock"));
        productPriceCol.setCellValueFactory(new PropertyValueFactory("price"));
        updateProductsTV();
    }
}
