package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Inhouse;
import static Model.Inventory.updatePart;
import Model.Outsourced;
import Model.Part;
import static Controller.MainScreenController.getSelectedPart;
import static Controller.MainScreenController.getSelectedPartIndex;
import Model.Inventory;

public class ModifyPartScreenController implements Initializable {

    @FXML
    private AnchorPane scrModifyPart;

    @FXML
    private RadioButton rbInHouse;

    @FXML
    private ToggleGroup tgSource;

    @FXML
    private RadioButton rbOutsourced;

    @FXML
    private Label lblModifyPartMachineId;

    @FXML
    private TextField txtModifyPartID;

    @FXML
    private TextField txtModifyPartName;

    @FXML
    private TextField txtModifyPartInv;

    @FXML
    private TextField txtModifyPartPrice;

    @FXML
    private TextField txtModifyPartMax;

    @FXML
    private TextField txtModifyPartMachineId;

    @FXML
    private TextField txtModifyPartMin;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private int partID;

    private int index;

    @FXML
    private boolean bOutSourced;

    @FXML
    private String partFieldError = new String();

    @FXML
    private String partPraseError = new String();

    @FXML
    public void setTgSource(ToggleGroup tgSource) {
        this.tgSource = tgSource;
        rbInHouse.setToggleGroup(tgSource);
        rbOutsourced.setToggleGroup(tgSource);
    }

    @FXML
    public void getSourceAction(ActionEvent event) throws Exception {
        if (tgSource.getSelectedToggle() == rbInHouse) {
            lblModifyPartMachineId.setText("Machine ID");
            txtModifyPartMachineId.setPromptText("Mach ID");
            bOutSourced = false;

        } else {
            lblModifyPartMachineId.setText("Company Name");
            txtModifyPartMachineId.setPromptText("Comp Nm");
            bOutSourced = true;
        }
    }

    @FXML
    public void handleSaveAction(ActionEvent event) {
        String name = txtModifyPartName.getText();
        String stockStr = txtModifyPartInv.getText();
        String priceStr = txtModifyPartPrice.getText();
        String minStr = txtModifyPartMin.getText();
        String maxStr = txtModifyPartMax.getText();
        String machineIdStr = txtModifyPartMachineId.getText();

        String partErrors = Part.partCheck(name, stockStr, priceStr, maxStr, minStr, machineIdStr);
        if (bOutSourced != true) {
            try {
                if (!machineIdStr.equals("")) {
                    Integer.parseInt(machineIdStr);
                }
            } catch (NumberFormatException e) {
                partErrors += "The Machine Id for In House parts must be a number. \n";
            }
        }

        if (partErrors.length() > 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Add Part Error");
            alert.setHeaderText("Part not added.");
            alert.setContentText(partErrors);
            alert.showAndWait();
            return;
        }

        int stock = Integer.parseInt(stockStr);
        double price = Double.parseDouble(priceStr);
        int min = Integer.parseInt(minStr);
        int max = Integer.parseInt(maxStr);

        if (bOutSourced == true) {

            if (Inventory.lookupPart(partID) instanceof Outsourced) {
                Outsourced updatedPart = (Outsourced) Inventory.lookupPart(partID);
                updatedPart.setName(name);
                updatedPart.setPrice(price);
                updatedPart.setStock(stock);
                updatedPart.setMin(min);
                updatedPart.setMax(max);
                updatedPart.setCompanyName(machineIdStr);
            } else {

                Outsourced updatedPart = new Outsourced(partID, name, price, stock, min, max, machineIdStr);
                updatePart(index, updatedPart);
            }
        } else {

            if (Inventory.lookupPart(partID) instanceof Inhouse) {
                Inhouse updatedPart = (Inhouse) Inventory.lookupPart(partID);
                updatedPart.setName(name);
                updatedPart.setPrice(price);
                updatedPart.setStock(stock);
                updatedPart.setMin(min);
                updatedPart.setMax(max);
                updatedPart.setMachineId(Integer.parseInt(machineIdStr));
            } else {

                Inhouse updatedPart = new Inhouse(partID, name, price, stock, min, max, Integer.parseInt(machineIdStr));
                updatePart(index, updatedPart);
            }
        }
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MainScreen.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage winMainScreen = (Stage) ((Node) event.getSource()).getScene().getWindow();
            winMainScreen.setTitle("Inventory Management System");
            winMainScreen.setScene(scene);
            winMainScreen.show();
        } catch (IOException e) {
        }
    }

    @FXML
    public void handleCancelAction(ActionEvent event) throws IOException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Cancel Modify Part");
            alert.setHeaderText("Are you sure you want to cancel? Click OK to exit. Click Cancel to continue.");
            alert.showAndWait();

            if (alert.getResult() == ButtonType.OK) {
                Parent root = FXMLLoader.load(getClass().getResource("/View/MainScreen.fxml"));
                Scene scene = new Scene(root);
                Stage winMainScreen = (Stage) ((Node) event.getSource()).getScene().getWindow();
                winMainScreen.setTitle("Inventory Management System");
                winMainScreen.setScene(scene);
                winMainScreen.show();
            } else {
                alert.close();
            }
        } catch (IOException e) {
        }
    }

    @FXML
    void setTGSource(ActionEvent event) {
        if (tgSource.getSelectedToggle() == rbInHouse) {
            lblModifyPartMachineId.setText("Machine ID");
            txtModifyPartMachineId.setPromptText("Mach ID");
            bOutSourced = false;
        } else {
            lblModifyPartMachineId.setText("Company Name");
            txtModifyPartMachineId.setPromptText("Comp Nm");
            bOutSourced = true;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Part selectedPart = getSelectedPart();
        partID = getSelectedPart().getId();
        index = getSelectedPartIndex();

        txtModifyPartID.setText("Auto Gen - Disabled");
        txtModifyPartName.setText(selectedPart.getName());
        txtModifyPartInv.setText(Integer.toString(selectedPart.getStock()));
        txtModifyPartPrice.setText(Double.toString(selectedPart.getPrice()));
        txtModifyPartMax.setText(Integer.toString(selectedPart.getMax()));
        txtModifyPartMin.setText(Integer.toString(selectedPart.getMin()));

        if (selectedPart instanceof Inhouse) {
            tgSource.selectToggle(rbInHouse);
            txtModifyPartMachineId.setText(Integer.toString(((Inhouse) selectedPart).getMachineId()));
            bOutSourced = false;
        } else {
            tgSource.selectToggle(rbOutsourced);
            txtModifyPartMachineId.setText(((Outsourced) selectedPart).getCompanyName());
            lblModifyPartMachineId.setText("Company Name");
            txtModifyPartMachineId.setPromptText("Comp Nm");
            bOutSourced = true;
        }
    }
}
