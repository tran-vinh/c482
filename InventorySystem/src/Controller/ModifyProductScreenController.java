package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Inventory;
import Model.Part;
import Model.Product;
import static Controller.MainScreenController.getSelectedProduct;
import static Controller.MainScreenController.getSelectedProductIndex;
import static Model.Inventory.getAllParts;
import static Model.Inventory.lookupPart;


public class ModifyProductScreenController implements Initializable {
    Product modifyProduct = new Product(getSelectedProduct());
        
    @FXML
    private int productID = getSelectedProduct().getId();

    @FXML
    private AnchorPane scrModifyProduct;

    @FXML
    private TextField txtModifyProductID;

    @FXML
    private TextField txtModifyProductName;

    @FXML
    private TextField txtModifyProductInv;

    @FXML
    private TextField txtModifyProductPrice;

    @FXML
    private TextField txtModifyProductMax;

    @FXML
    private TextField txtModifyProductMin;

    @FXML
    private Button btnSearch;

    @FXML
    private TextField txtSearchParts;

    @FXML
    private TableView<Part> tvPartsInStock;

    @FXML
    private TableColumn<Part, Integer> partIDCol;

    @FXML
    private TableColumn<Part, String> partNameCol;

    @FXML
    private TableColumn<Part, Integer> partInStockCol;

    @FXML
    private TableColumn<Part, Double> partPriceCol;

    @FXML
    private Button btnAdd;

    @FXML
    private TableView<Part> tvAssociatedParts;

    @FXML
    private TableColumn<Part, Integer> associatedPartIDCol;

    @FXML
    private TableColumn<Part, String> associatedPartNameCol;

    @FXML
    private TableColumn<Part, Integer> associatedPartInStockCol;

    @FXML
    private TableColumn<Part, Double> associatedPartPriceCol;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;
    
    @FXML
    private String productParseErrors = new String();
    
    @FXML
    private String productFieldErrors = new String();
    
    @FXML
    public void handleSearchAction(ActionEvent event) {
        String searchPartStr = txtSearchParts.getText();
        ObservableList<Part> searchPartList = FXCollections.observableArrayList();
        try{
            searchPartList.add(lookupPart(Integer.parseInt(searchPartStr)));
            tvPartsInStock.setItems(searchPartList);
        }
        catch(NumberFormatException e){
            searchPartList.add(lookupPart(searchPartStr));
            tvPartsInStock.setItems(searchPartList);
        }
        finally {
            if(searchPartStr.equals("")) {
                tvPartsInStock.setItems(getAllParts());
            }
        }
    }
    
    @FXML
    public void handleAddAction(ActionEvent event) {
        Part part = tvPartsInStock.getSelectionModel().getSelectedItem();
        modifyProduct.addAssociatedPart(part);
        tvAssociatedParts.setItems(modifyProduct.getAllAssociatedParts());
    }
    
    @FXML
    public void handleDeleteAction(ActionEvent event) {
        if (tvAssociatedParts.getSelectionModel().getSelectedIndex() != -1) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Associated Part");
            alert.setHeaderText("Are you sure you want to delete the associated part? Click OK to delete the associated part. Click Cancel to return to product screen.");
            alert.showAndWait();
            if (alert.getResult() == ButtonType.OK) {
                Part part = tvAssociatedParts.getSelectionModel().getSelectedItem();
                modifyProduct.deleteAssociatedPart(part);
                tvAssociatedParts.setItems(modifyProduct.getAllAssociatedParts());
            }
            else {
                alert.close();
            }
        }
    }
    
    @FXML
    public void handleSaveAction(ActionEvent event) throws IOException {
        String name = txtModifyProductName.getText();
        String stock = txtModifyProductInv.getText();
        String price = txtModifyProductPrice.getText();
        String max = txtModifyProductMax.getText();
        String min = txtModifyProductMin.getText();
        
        String productErrors = Product.productCheck(name, stock, price, max, min);
        
        if (tvAssociatedParts.getItems().isEmpty()) {
            productErrors += "No associated parts.\n";
        }
        
        if (productErrors.length() > 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Modify Product Error");
            alert.setHeaderText("Product not saved.");
            alert.setContentText(productErrors);
            alert.showAndWait();
            return;
            
        }else {
            modifyProduct.setName(name);
            modifyProduct.setStock(Integer.parseInt(stock));
            modifyProduct.setPrice(Double.parseDouble(price));
            modifyProduct.setMax(Integer.parseInt(max));
            modifyProduct.setMin(Integer.parseInt(min));
            Inventory.updateProduct(getSelectedProductIndex(), modifyProduct);
            try{
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MainScreen.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage winMainScreen = (Stage)((Node)event.getSource()).getScene().getWindow();
                winMainScreen.setTitle("Inventory Management System");
                winMainScreen.setScene(scene);
                winMainScreen.show();
            }catch(IOException e) {}
        }
    }
    
    @FXML
    public void handleCancelAction (ActionEvent event) throws IOException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Cancel Modify Product");
            alert.setHeaderText("Are you sure you want to cancel? Click OK to exit to the Main screen. Click Cancel to stay.");
            alert.showAndWait();
            
            if (alert.getResult() == ButtonType.OK) {
                Parent root = FXMLLoader.load(getClass().getResource("/View/MainScreen.fxml"));
                Scene scene = new Scene(root);        
                Stage winMainScreen = (Stage)((Node)event.getSource()).getScene().getWindow();        
                winMainScreen.setTitle("Inventory Management System");
                winMainScreen.setScene(scene);
                winMainScreen.show();
            }
            else {
                alert.close();
            }
        }
        catch (IOException e) {}
    }
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtModifyProductID.setText("Auto Gen - Disabled ");
        txtModifyProductName.setText(modifyProduct.getName());
        txtModifyProductInv.setText(Integer.toString(modifyProduct.getStock()));
        txtModifyProductPrice.setText(Double.toString(modifyProduct.getPrice()));
        txtModifyProductMax.setText(Integer.toString(modifyProduct.getMax()));
        txtModifyProductMin.setText(Integer.toString(modifyProduct.getMin()));  
        
        partIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        tvPartsInStock.setItems(getAllParts());
        
        associatedPartIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        associatedPartNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        associatedPartInStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        associatedPartPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        tvAssociatedParts.setItems(modifyProduct.getAllAssociatedParts());
    }    

}
