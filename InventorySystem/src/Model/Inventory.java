package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Inventory {
    private static ObservableList<Product> allProducts = FXCollections.observableArrayList();
    private static ObservableList<Part> allParts = FXCollections.observableArrayList();
    private static int partId = 0;
    private static int productId = 0;

    public static void addPart(Part newPart) {
        allParts.addAll(newPart);
    }   
    
    public static void addProduct(Product newProduct) {
        allProducts.add(newProduct);
    }
    
    public static Part lookupPart(int partId) {
        for(Part foundPart : allParts) {
            if(foundPart.getId() == partId) {
                return foundPart;
            }
        }
        return null;
    }
    
    public static Product lookupProduct(int productId) {
        for (Product foundProduct : allProducts) {
            if (foundProduct.getId() == productId) {
                return foundProduct;
            }
        }
        return null;
    }  
    
    public static Part lookupPart(String partName) {
        for(Part foundPart : allParts) {
            if(foundPart.getName().equals(partName))
                return foundPart;
        }
        return null;
    }
    
    public static Product lookupProduct(String productName) {
        for (Product foundProduct : allProducts) {
            if (foundProduct.getName().equals(productName)) {
                return foundProduct;
            }
        }
        return null;
    }    
    
    public static void updatePart(int index, Part selectedPart) {
        allParts.set(index, selectedPart);
    }    
    
    public static void updateProduct(int index, Product newProduct) {
        allProducts.set(index, newProduct);
    }    
    
    public static boolean deletePart(Part selectedPart) {
        return allParts.remove(selectedPart);
    }        
        
    public static boolean deleteProduct(Product selectedProduct) {
        return allProducts.remove(selectedProduct);
    }
    
    public static ObservableList<Part> getAllParts() {
        return allParts;
    }
    
    public static ObservableList<Product> getAllProducts() {
        return allProducts;
    }
    
    
    //Added functions to keep track of IDs
    public static int nextPartId() {
        return ++partId;
    }
    
    public static int nextProductId() {
        return ++productId;
    }
}
