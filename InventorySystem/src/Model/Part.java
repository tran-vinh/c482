package Model;

public abstract class Part {

    protected int id = 0;
    protected String name = "";
    protected double price = 0;
    protected int stock = 0;
    protected int min = 0;
    protected int max = 0;
    
    public Part(int id, String name, double price, int stock, int min, int max) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.min = min;
        this.max = max;        
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public double getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    
    //Functions for Set 1&2 exception control
    public static String partCheck(String name, String stockStr, String priceStr, String maxStr, String minStr, String machineId) {
        String partError = "";
        
        if (name.equals("")) {
            partError = partError + "The Name cannot be empty. \n";
        } 
        
        if (stockStr.equals("")) {
            partError = partError + "The Inventory cannot be empty. \n";
        } else{
            try {
                Integer.parseInt(stockStr);
            } catch (NumberFormatException e) {
                partError = partError + "The Inventory is invalid. \n";
            }     
        }
        
        if (priceStr.equals("")) {
            partError = partError + "The Price cannot be empty. \n";
        } else{        
            try {
                Double.parseDouble(priceStr);
            } catch (NumberFormatException e) {
                partError = partError + "The Price is invalid. \n";
            }     
        }
        
        if (minStr.equals("")) {
            partError = partError + "The Min cannot be empty. \n";
        } else{   
            try {
                Integer.parseInt(minStr);
            } catch (NumberFormatException e) {
                partError = partError + "The Min is invalid. \n";
            }
        }
        
        if (maxStr.equals("")) {
            partError = partError + "The Max cannot be empty. \n";
        } else{
            try {
                Integer.parseInt(maxStr); 
            } catch (NumberFormatException e) {
                partError = partError + "The Max is invalid. \n";
            }     
        }
        if (machineId.equals("")) {
            partError = partError + "The MachineId or Company Name cannot be empty. \n";
        }
        
        //check stock is less than max
        try {
            int stock = Integer.parseInt(stockStr);
            int max = Integer.parseInt(maxStr); 
            if (stock > max) {
                partError = partError + "The inventory cannot exceed the maximum stock. \n";
            }
        } catch (NumberFormatException e) {/*do nothing if cannot parse*/} 
        
        //Check max is greater than min
        try {
            int min = Integer.parseInt(minStr);
            int max = Integer.parseInt(maxStr); 
            if (max < min) {
                partError = partError + "The maximum stock must exceed the minimum stock. \n";
            }
        } catch (NumberFormatException e) {/*do nothing if cannot parse*/}   
        
        //Check stock is greater than min 
        try {
            int stock = Integer.parseInt(stockStr);
            int min = Integer.parseInt(minStr); 
            if (stock < min) {
                partError = partError + "The inventory cannot be less than the minimum stock. \n";
            }
        } catch (NumberFormatException e) {/*do nothing if cannot parse*/}               

        
        return partError;
    }
}
