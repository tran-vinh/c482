package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Product {
    private ObservableList<Part> associatedParts = FXCollections.observableArrayList();;
    private int id = 0;
    private String name = "";    
    private double price = 0;    
    private int stock = 0;
    private int min = 0;
    private int max = 0;
    
    public Product() {}
            
    //copy constructor for modify product
    public Product (Product source) {
        this.associatedParts = FXCollections.observableArrayList(source.associatedParts);
        this.id = source.id;
        this.name = source.name;
        this.price = source.price;
        this.stock = source.stock;
        this.min = source.min;
        this.max = source.max;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }

    public int getMin() {
        return min;
    }
    
    public int getMax() {
        return max;
    }
    
    public void addAssociatedPart(Part part){
        associatedParts.add(part);
    }
    
    public boolean deleteAssociatedPart(Part selectedAssociatedPart) {
        return associatedParts.remove(selectedAssociatedPart);
    }
    
    public ObservableList getAllAssociatedParts() {
        return associatedParts;
    }
    
    
    //Functions for Set 1&2 exception control
    public static String productCheck(String name, String stockStr, String priceStr, String maxStr, String minStr) {
        String productError = "";
        
        if (name.equals("")) {
            productError = productError + "The Name cannot be empty. \n";
        } 
        
        if (stockStr.equals("")) {
            productError = productError + "The Inventory cannot be empty. \n";
        } else{
            try {
                Integer.parseInt(stockStr);
            } catch (NumberFormatException e) {
                productError = productError + "The Inventory is invalid. \n";
            }     
        }
        
        if (priceStr.equals("")) {
            productError = productError + "The Price cannot be empty. \n";
        } else{        
            try {
                Double.parseDouble(priceStr);
            } catch (NumberFormatException e) {
                productError = productError + "The Price is invalid. \n";
            }     
        }
        
        if (minStr.equals("")) {
            productError = productError + "The Min cannot be empty. \n";
        } else{   
            try {
                Integer.parseInt(minStr);
            } catch (NumberFormatException e) {
                productError = productError + "The Min is invalid. \n";
            }
        }
        
        if (maxStr.equals("")) {
            productError = productError + "The Max cannot be empty. \n";
        } else{
            try {
                Integer.parseInt(maxStr); 
            } catch (NumberFormatException e) {
                productError = productError + "The Max is invalid. \n";
            }     
        }
        
        //check stock is less than max
        try {
            int stock = Integer.parseInt(stockStr);
            int max = Integer.parseInt(maxStr); 
            if (stock > max) {
                productError = productError + "The inventory cannot exceed the maximum stock. \n";
            }
        } catch (NumberFormatException e) {/*do nothing if cannot parse*/} 
        
        //Check max is greater than min
        try {
            int min = Integer.parseInt(minStr);
            int max = Integer.parseInt(maxStr); 
            if (max < min) {
                productError = productError + "The maximum stock must exceed the minimum stock. \n";
            }
        } catch (NumberFormatException e) {/*do nothing if cannot parse*/}   
        
        //Check stock is greater than min 
        try {
            int stock = Integer.parseInt(stockStr);
            int min = Integer.parseInt(minStr); 
            if (stock < min) {
                productError = productError + "The inventory cannot be less than the minimum stock. \n";
            }
        } catch (NumberFormatException e) {/*do nothing if cannot parse*/}               

        
        return productError;
    }
}
    
